-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 26, 2017 at 09:34 AM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 7.0.19-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zork`
--

--
-- Truncate table before insert `CAMERE`
--

TRUNCATE TABLE `CAMERE`;
--
-- Dumping data for table `CAMERE`
--

INSERT INTO `CAMERE` (`id_camera`, `id_camera_N`, `id_camera_S`, `id_camera_E`, `id_camera_V`) VALUES
(1, 3, NULL, NULL, 2),
(2, NULL, NULL, 1, NULL),
(3, 5, 1, 4, NULL),
(4, 11, NULL, 10, 3),
(5, 8, 3, NULL, 6),
(6, NULL, NULL, 5, NULL),
(7, NULL, NULL, 8, NULL),
(8, 9, 5, NULL, 7),
(9, NULL, 8, 17, NULL),
(10, NULL, NULL, NULL, 4),
(11, 13, 4, 12, NULL),
(12, NULL, NULL, NULL, 11),
(13, 15, 11, 14, NULL),
(14, NULL, NULL, NULL, 13),
(15, 17, 13, 16, NULL),
(16, NULL, NULL, NULL, 15),
(17, NULL, 15, 18, 9),
(18, NULL, NULL, NULL, 17);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
