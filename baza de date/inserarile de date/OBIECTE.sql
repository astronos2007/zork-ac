-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 26, 2017 at 09:34 AM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 7.0.19-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zork`
--

--
-- Truncate table before insert `OBIECTE`
--

TRUNCATE TABLE `OBIECTE`;
--
-- Dumping data for table `OBIECTE`
--

INSERT INTO `OBIECTE` (`id_obiect`, `nume`, `descriere`, `id_camera`) VALUES
(1, 'dvd', 'Un DVD strălucitor cu Windows Vista. Ce este ciudat este că pare nou, nefolosit vreodată.', 7),
(2, 'doza', 'Doză de Coca-Cola aproape goală. Au rămas urme de ruj pe ea.', 2),
(3, 'tastatura', 'Tastatură MAGUAY, cu mufă USB lipsă, doar niște fire atârnând în voia cea bună, pe post de „connector”.', 12),
(4, 'guma', 'Gumă de mestecat, marcă proprie AC', 5),
(5, 'napolitane', 'Un pachet de napolitane JOE, dar cu o culoare ciudată, care seamănă cu chihlimbarul.', 5),
(6, 'hartie', 'Un sul de hârtie igienică, slavă Domnului, nefolosită.', 17),
(7, 'perie', 'Perie de păr, cu câteva fire atârnând pe ea. Poate fi folosită pe post de probe la criminalistică.', 13),
(8, 'pix', 'Cuvântul „BIC” iese în evidență pe acest pix strălucitor și... gol.', 13);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
