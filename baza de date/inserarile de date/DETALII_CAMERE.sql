-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 26, 2017 at 09:34 AM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 7.0.19-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zork`
--

--
-- Truncate table before insert `DETALII_CAMERE`
--

TRUNCATE TABLE `DETALII_CAMERE`;
--
-- Dumping data for table `DETALII_CAMERE`
--

INSERT INTO `DETALII_CAMERE` (`id_camera`, `descriere`, `descriere_usa`, `etaj`) VALUES
(1, 'Intrarea principală a Facultății de Automatică și Calculatoare. Un hol mare, prin care, de obicei, circulă foarte mulți studenți.', 'Se vede un hol foarte mare, cu câteva scări pe lângă, care par să ducă la alt etaj. Se zărește și o intrare cu uși glisante.', 'P'),
(2, 'Camera cu magazinul facultății, cu diverse bunătăți, de la dulciuri, până la cafele și băuturi. În fața magazinului este și un automat de cafea pentru cei disperați, sau slabi de înger.', 'Se zărește o cameră foarte mică, cu un aparat dubios și înalt, care seamănă cu un robot din Star Trek. Se mai văd și niște vitrine cu pufuleți expuși.', 'P'),
(3, 'O bifurcație într-un hol, care duce ori înspre două amfiteatre mirifice, ori înspre așa-numitele „borcane”. Spre vest avem și câteva scări. Cine știe unde or duce...', 'O cale care se împarte deodată în două, din motive necunoscute de proiectare a clădirii. Probabil duce către viața de apoi, respectiv viața de vis...', 'P'),
(4, 'Un minunat „început de hol” cu pereți de sticlă spre est și pereți masivi spre vest. În partea de est, se vede o ușă masivă de metal verde, pe care scrie „C0-2”.', 'Se vede un hol lung ce seamănă cu holurile din castelul Hogwarts, doar că prezintă mai mult material sticlos.', 'P'),
(5, 'Prima parte a holului care găzduiește, spre vest, amfiteatrele iubitei noastre facultăți.', 'Un hol decent, foarte spațios, cu pereți ovali de sticlă spre est și cu două uși de lemn spre vest.', 'P'),
(6, 'Amfiteatrul AC0-1, mult iubit de studenții care vin (sau nu) la cursuri. Necunoscut de studenții de la TI, dar arhicunoscut de cei de la C, respectiv IS.', 'Se vede o ușă pe care scrie „Amfiteatrul AC0-1”. E atât de liniște însă, încât probabil încă se dă examenul de MN.', 'P'),
(7, 'Mult-iubitul amfiteatru AC0-2, îndrăgit de studenții de la TI, în special, pentru că doar aici funcționează laptopul d-lui prof. Manta, pus la proiector...', 'Se vede o ușă pe care scrie „Amfiteatrul AC0-2”. Posibil a fi părăsit, deoarece se observă pe orarul lipit pe ușă „Curs SD - ora 08:00”.', 'P'),
(8, 'A doua parte a holului, care găzduiește, spre vest, amfiteatrele iubitei noastre facultăți.', 'Un hol decent, foarte spațios, cu pereți ovali de sticlă spre est și cu două uși de lemn spre vest. Spre nord, se zărește un set de scări masive, care seamănă leit cu pereții.', 'P'),
(9, 'Un hol care face trecerea între atmosfera de la băi și atmosfera elevată de urcare la etajul 1.', 'Se vede un set de scări masive, ce duce spre necunoscut.', 'P'),
(10, 'Borcanul C0-2. Multe calculatoare, o tablă și o catedră.', 'Pe ușa din metal verde masiv, pe lângă care dăiuniesc niște pereți de sticlă, scrie „Laborator C0-2”.', 'P'),
(11, 'Holul prin care se ajunge în borcane, cu pereți de sticlă spre est și pereți masivi spre vest. În partea de est, se vede o ușă masivă de metal verde, pe care scrie „C0-3”.', 'Se vede un hol lung ce seamănă cu holurile din castelul Hogwarts, doar că prezintă mai mult material sticlos.', 'P'),
(12, 'Borcanul C0-3. Multe calculatoare, o tablă și o catedră.', 'Pe ușa din metal verde masiv, pe lângă care dăiuniesc niște pereți de sticlă, scrie „Laborator C0-3”.', 'P'),
(13, 'În partea de est, se vede o ușă masivă de metal verde, pe care scrie „C0-4”.', 'Se vede un hol lung ce seamănă cu holurile din castelul Hogwarts, doar că prezintă mai mult material sticlos.', 'P'),
(14, 'Borcanul C0-4. Multe calculatoare, o tablă și o catedră.', 'Pe ușa din metal verde masiv, pe lângă care dăiuniesc niște pereți de sticlă, scrie „Laborator C0-4”.', 'P'),
(15, 'În partea de est, se vede o ușă masivă de metal verde, pe care scrie „C0-5”. Spre nord, se văd niște intrări către tronurile meditației (toaletele).', 'Un hol decent, foarte spațios, cu pereți ovali de sticlă spre est și cu două uși de lemn spre vest.', 'P'),
(16, 'Borcanul C0-5. Multe calculatoare, o tablă și o catedră.', 'Pe ușa din metal verde masiv, pe lângă care dăiuniesc niște pereți de sticlă, scrie „Laborator C0-5”.', 'P'),
(17, 'O bucățică de hol care servește ca interfață între toalete și calea către educație. Spre est, în fața toaletelor, este o ușă pe care scrie „C0-6”, iar spre vest se zărește un hol minuscul cu niște scări masive.', 'Se zărește un holișor mic cu două intrări către două camere pe care scrie „Bărbați”, respectiv „Femei”, plus încă o ușă dubioasă din metal verde masiv.', 'P'),
(18, 'Borcanul C0-6. Multe calculatoare, o tablă și o catedră.', 'Pe ușa din metal verde masiv, pe lângă care dăiuniesc niște pereți de sticlă, scrie „Laborator C0-6”.', 'P');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
