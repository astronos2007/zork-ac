An implementation of a Zork game in C, using a MySQL Database for all actions.

Any step made in game results in a SQL command given to a database server, 
which returns a response to the C application.

Dependencies: MySQL Database, MySQL Connector, MySQL C API