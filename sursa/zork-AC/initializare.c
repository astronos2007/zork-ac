//
// Created by cosmin on 21.05.2017.
//

#include <stdio.h>
#include <mysql.h>
#include <stdlib.h>
#include <string.h>

#include "initializare.h"
#include "baza_de_date.h"
#include "zork_utils.h"

// functie de initializare care logheaza / inregistreaza utilizatorul
// returneaza ID-ul utilizatorului, odata logat / inregistrat
long init(MYSQL* conn)
{


    int optiune_ok;
    int opt;

    meniu:
    afisare_logo();
    printf(CYN "Bun venit in lumea Zork AC, o lume plină de... studenți, profesori, camere și obiecte.\n\n" RESET);

    do {
        optiune_ok = 1;
        printf("Alegeți una din opțiuni:\n");
        printf("1. Înregistrare utilizator nou\n");
        printf("2. Autentificare utilizator existent\n");
        printf("3. Modificare parolă\n");
        printf(RED "4. Ștergere cont\n" RESET);
        printf("5. Ieșire din joc\n");

        scanf("%d", &opt);
        flush_input();
        if (opt < 1 || opt > 5)
        {
            optiune_ok = 0;
            printf(RED "Alegeți o opțiune validă!\n\n" RESET);
        }
    } while(!optiune_ok);

    afisare_logo();

    // credentiale utilizator
    long id_user = 0;
    char username[MAX_USER_LENGTH + 1];
    char parola[MAX_PASSWORD_LENGTH + 1];
    char nume[MAX_NAME_LENGTH + 1];
    char prenume[MAX_SURNAME_LENGTH + 1];

    int user_ok;
    int creds_ok;

    switch (opt)
    {
        case 1: // inregistrare utilizator nou
            // preluam datele de la tastatura
            printf("Introduceti numele de familie (maxim 45 de caractere): ");
            fgets(nume, MAX_NAME_LENGTH, stdin);
            nume[strlen(nume) - 1] = '\0';

            printf("Introduceti prenumele (maxim 45 de caractere): ");
            fgets(prenume, MAX_SURNAME_LENGTH, stdin);
            prenume[strlen(prenume) - 1] = '\0';

            do {
                user_ok = 1;

                printf("Introduceți numele de utilizator (maxim 25 de caractere): ");
                fgets(username, MAX_USER_LENGTH, stdin);
                username[strlen(username) - 1] = '\0';

                // verificare unicitate utilizator
                sprintf(text_interogare, "SELECT id_user "
                                        "FROM UTILIZATORI "
                                        "WHERE user='%s'", username);
                rez_inter = interogare(conn, text_interogare);
                if (mysql_num_rows(rez_inter) != 0)
                {
                    printf(RED "Numele de utilizator există deja în baza de date! " RESET "Alegeți un altul.\n");
                    user_ok = 0;
                }
                mysql_free_result(rez_inter);
            } while(!user_ok);

            printf("Introduceți parola (maxim 20 de caractere): ");
            fgets(parola, MAX_PASSWORD_LENGTH, stdin);
            parola[strlen(parola) - 1] = '\0';

            // le introducem in baza de date
            sprintf(text_interogare, "INSERT INTO UTILIZATORI (nume, prenume, user, parola, orientare, id_camera) "
                                    "VALUES ('%s', '%s', '%s', '%s', 'N', 1)", nume, prenume, username, parola);
            rez_inter = interogare(conn, text_interogare);
            mysql_free_result(rez_inter);

            // preluam ID-ul utilizatorului nou creat
            sprintf(text_interogare, "SELECT id_user "
                                    "FROM UTILIZATORI "
                                    "WHERE user='%s'", username);
            rez_inter = interogare(conn, text_interogare);
            rand_inter = mysql_fetch_row(rez_inter);
            id_user = strtol(rand_inter[0], &cptr_inter, 10);
            mysql_free_result(rez_inter);

            printf(BLU "\nÎnregistrare efectuată cu succes! Bun venit în lumea Zork, %s %s!\n" RESET, prenume, nume);

            break;

        case 2: // logare utilizator existent
            do {
                creds_ok = 1;

                printf("Introduceți numele de utilizator: ");
                fgets(username, MAX_USER_LENGTH, stdin);
                username[strlen(username) - 1] = '\0';

                printf("Introduceți parola: ");
                fgets(parola, MAX_PASSWORD_LENGTH, stdin);
                parola[strlen(parola) - 1] = '\0';

                // verificam credentialele de logare
                sprintf(text_interogare, "SELECT id_user "
                                        "FROM UTILIZATORI "
                                        "WHERE user='%s' AND parola='%s'", username, parola);
                rez_inter = interogare(conn, text_interogare);
                if (mysql_num_rows(rez_inter) == 0)
                {
                    printf(RED "Eroare!" RESET " Nume de utilizator sau parolă greșită!\n");
                    creds_ok = 0;
                }
                else
                {
                    rand_inter = mysql_fetch_row(rez_inter);
                    id_user = strtol(rand_inter[0], &cptr_inter, 10);
                }
                mysql_free_result(rez_inter);
            } while (!creds_ok);
            break;

        case 3: // actualizare parola
            do {
                creds_ok = 1;

                printf("Introduceți numele de utilizator: ");
                fgets(username, MAX_USER_LENGTH, stdin);
                username[strlen(username) - 1] = '\0';

                printf("Introduceți parola actuală: ");
                fgets(parola, MAX_PASSWORD_LENGTH, stdin);
                parola[strlen(parola) - 1] = '\0';

                // verificam credentialele de logare
                sprintf(text_interogare, "SELECT id_user "
                                        "FROM UTILIZATORI "
                                        "WHERE user='%s' AND parola='%s'", username, parola);
                rez_inter = interogare(conn, text_interogare);
                if (mysql_num_rows(rez_inter) == 0)
                {
                    printf(RED "Eroare!" RESET " Nume de utilizator sau parolă actuală greșită!\n");
                    creds_ok = 0;
                    mysql_free_result(rez_inter);
                }
            } while (!creds_ok);

            rand_inter = mysql_fetch_row(rez_inter);
            id_user = strtol(rand_inter[0], &cptr_inter, 10);
            mysql_free_result(rez_inter);

            printf("Introduceți noua parolă: ");
            fgets(parola, MAX_PASSWORD_LENGTH, stdin);
            parola[strlen(parola) - 1] = '\0';

            // actualizam parola in baza de date
            sprintf(text_interogare, "UPDATE UTILIZATORI "
                                    "SET parola='%s' "
                                    "WHERE id_user=%ld", parola, id_user);
            rez_inter = interogare(conn, text_interogare);
            mysql_free_result(rez_inter);

            printf(BLU "Parola a fost actualizată cu succes!" RESET " Apăsați ENTER pentru a reveni la meniul principal...");
            getchar();
            system("clear");
            goto meniu;

        case 4: // stergere cont
            do {
                creds_ok = 1;

                printf("Introduceți numele de utilizator: ");
                fgets(username, MAX_USER_LENGTH, stdin);
                username[strlen(username) - 1] = '\0';

                printf("Introduceți parola actuală: ");
                fgets(parola, MAX_PASSWORD_LENGTH, stdin);
                parola[strlen(parola) - 1] = '\0';

                // verificam credentialele de logare
                sprintf(text_interogare, "SELECT id_user "
                                        "FROM UTILIZATORI "
                                        "WHERE user='%s' AND parola='%s'", username, parola);
                rez_inter = interogare(conn, text_interogare);
                if (mysql_num_rows(rez_inter) == 0)
                {
                    printf(RED "Eroare!" RESET " Nume de utilizator sau parolă actuală greșită!\n");
                    creds_ok = 0;
                    mysql_free_result(rez_inter);
                }

                if (creds_ok)
                {
                    rand_inter = mysql_fetch_row(rez_inter);
                    id_user = strtol(rand_inter[0], &cptr_inter, 10);
                    printf("Sigur doriți să vă ștergeți definitiv contul? Introduceți \"" RED "da" RESET "\" pentru confirmare: ");
                    char st_acc[5];
                    fgets(st_acc, MAX_CHAR_WIDTH - 1, stdin);
                    st_acc[strlen(st_acc) - 1] = '\0';
                    if (strcmp(st_acc, "da") == 0)
                    {
                        sprintf(text_interogare, "DELETE FROM UTILIZATORI "
                                "WHERE id_user=%ld", id_user);
                        rez_inter = interogare(conn, text_interogare);
                        mysql_free_result(rez_inter);

                        printf(BLU "Contul dvs. a fost șters cu succes." RESET " Apăsați ENTER pentru a reveni la meniul principal...\n");
                        getchar();
                        system("clear");
                    }
                }
            } while (!creds_ok);
            goto meniu;

        case 5: // iesire din program
            deconectare_BD(conn);
            system("clear");
            exit(EXIT_SUCCESS);
        default:
            break;
    }

    return id_user;
}

void afisare_logo()
{
    system("clear");
    printf(RED "******************************************************************************************\n" RESET YEL
           " ``//++o:+osssys+mmNNm``  .:ymyonnsoyyhy    .`````:////:++/:-.```    .yyyys/       .....` \n"
           " :///++o:+osssys+mmNNmd ydsdoyhmNydhsmmdy/.`` +MMMMMmmMMMMMNoh:``    /NmNmm/     .:......`\n"
           "--::://++oosyhdmNMMMMd mMMMMMMNm   dNNdmmmy- :dMMMMMmNMMMMhMMMNdm:` `omsmmd`   .sysooo+`  \n"
           "````......--/syshdddmh mNmmhdhh     MNMMmNm/ :+hmNMMMmMMMNmMNmNMMs```hNdddh.  `+o+:+++.   \n"
           "           :yhh+hdmNh-.hmhNymmh     NNNNNNNN dmNNNmmm+   :dmNMMMMo``.MMNNmh` .yhyyso/     \n"
           "        `:osyyhdmNd/``.dNhMhmmh     MMNNMMmh sNMMMMMM     hNmmmmm:```hdhhhy``/+++o+/`     \n"
           "       `://+osydh/````.dNhMhmmd     MMNNNNm/ hMMNNNNd:-/hNMMMMMMM-``:MNmhyssoo++//.       \n"
           "    `.-::///+o:     ```yNhNhmmd     MMNMMNd/ dMMMMMMN:-hMMMMMMMNs.``.ddhyoo+//::--.       \n"
           "  `.-----::/:.       ``omymydmyN   mMMNNNMNm hNNNNNNo   MMMNNNmo.````NMNmhy-.+++++//-     \n"
           " `......---`````..`  ../hsyshdsmh mdMNmhNNd/ hmmmmNM     hNMMMMMo```:MMNdhy- :///////-    \n"
           "`...`.-:.:///+ooo      /smmhssmys+yoyhyhdmy. oMMhdNNo    :dmmmmmy`  -yyoo+-   `.--..-.`   \n"
           " ``````...`..--.--:-:--  /oyyhhy+/+ohyhyyo`/ NNdhNNh      dNoNNNd`   /s:++/`    `::..--.  \n"
           "  ```.```..... `------.`.`--:/+o/::/+os+o+.` `-osooso:     /sssys+`  .::::-`     `......` \n"
           RED "******************************************************************************************" RESET);
    printf(RESET "\n\n");
}