//
// Created by cosmin on 21.05.2017.
//

#include <stdlib.h>
#include <stdio.h>
#include <mysql.h>

#include "baza_de_date.h"
#include "zork_utils.h"

// functia de conectare la baza de date
MYSQL* conectare_BD()
{
    MYSQL *conn; // variabila care retine conexiunea cu baza de date

    char* server = "localhost";
    char* utilizator = "cosmin";
    char* parola = "inscriere";
    char* baza_de_date = "zork";

    conn = mysql_init(NULL);

    // conectare la baza de date
    if (!mysql_real_connect(conn, server,
                            utilizator, parola, baza_de_date, 0, NULL, 0))
    {
        eroare(conn);
    }

    mysql_set_character_set(conn, "utf8");

    return conn;
}

// functia de deconectare de la baza de date
void deconectare_BD(MYSQL* conn)
{
    // system("clear");
    mysql_close(conn);
}

// functie care inchide programul si conexiunea cu baza de date cand se genereaza eroare
void eroare(MYSQL* conn)
{
    fprintf(stderr, RED "Eroare!" RESET " Mesajul raportat de MySQL este: " MAG "%s\n" RESET, mysql_error(conn));
    deconectare_BD(conn);
    exit(EXIT_FAILURE);
}

// folosite pentru interogari
char text_interogare[MAX_QUERY_LENGTH];
MYSQL_RES* rez_inter;
MYSQL_ROW rand_inter;

// functie care executa o interogare si returneaza rezultatul sub forma de randuri iterabile
MYSQL_RES* interogare(MYSQL* conn, const char* cod_interogare)
{
    MYSQL_RES* res;
    if (mysql_query(conn, cod_interogare))
    {
        eroare(conn);
    }
    res = mysql_store_result(conn);
    return res;
}
