//
// Created by cosmin on 22.05.2017.
//

#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>
#include <string.h>
#include "baza_de_date.h"
#include "zork_utils.h"
#include "accesori.h"
#include "miscare.h"

// folosite pentru interogari
char text_interogare[MAX_QUERY_LENGTH];
MYSQL_RES* rez_inter;
MYSQL_ROW rand_inter;

// elibereaza buffer-ul de intrare (stdin) in cazul unei comenzi invalid introduse
void flush_input()
{
    char c;
    while((c = getchar()) != '\n' && c != EOF);
}

void joc(MYSQL* conn, long id_user)
{
    printf(GRN "Autentificare reușită! " RESET " Apăsați ENTER pentru a începe jocul...\n");
    getchar();
    system("clear");

    // mesaje de bun venit
    printf("******************************************************************************************\n");
    printf("Bine ați venit la " CYN "ZORK (AC)" RESET ", un joc foarte simplu în linie de comandă!\n"
                   "Tot ce trebuie să faceți este să explorați minunata lume a Facultății de Automatică și\n"
                   "Calculatoare și să găsiți obiectele care vor ajuta la terminarea jocului. AC-ul este un\n"
                   "labirint în adevăratul sens al cuvântului, așa că este foarte ușor să te pierzi prin\n"
                   "toate camerele disponibile.\n");
    printf("******************************************************************************************\n\n");
    printf("Apăsați " BLU "'h' + ENTER" RESET " pentru a afișa meniul cu comenzile disponibile.\n\n");

    int id_camera = preluare_id_camera(conn, id_user);
    mesaj_bun_venit(conn, id_user);
    printf("Vă aflați în camera " YEL "%d" RESET ". (folosiți " BLU "'c' + ENTER + dsc" RESET " pentru a afla detalii).\n\n", id_camera);

    int c;
    char comanda[MAX_CHAR_WIDTH];
    do {
        printf("zk (" YEL "D" RESET ") >> ");
        c = getchar();
        if (getchar() != '\n') // urmatorul caracter nu e ENTER
        {
            // comanda invalida!
            flush_input();
            c = '\0';
        }

        switch (c) {
            case 'c': // modul de COMANDA
                do {
                    printf("zk (" BLU "C" RESET ") >> ");
                    preluare_comanda(comanda);
                    if (procesare_comanda(conn, comanda, id_user) == -1)
                    {
                        // iesire din joc
                        c = 'q';
                        break;
                    }
                } while(strcmp(comanda, "depl"));
                break;
            case 'w': // mergi inainte
                inainte(conn, id_user);
                break;
            case 's': // mergi inapoi
                inapoi(conn, id_user);
                break;
            case 'd': // indreapta-te spre dreapta
                dreapta(conn, id_user);
                break;
            case 'a': // indreapta-te spre stanga
                stanga(conn, id_user);
                break;
            case 'k': // afisare orientare
                afisare_orientare(conn, id_user);
                break;
            case 'h': // ajutor
                afisare_ajutor();
                break;
            case 'q': // iesire din program
                printf("Sigur doriți să ieșiți din joc? Starea dvs. actuală este salvată automat. Introduceți \"" RED "da" RESET "\" pentru confirmare: ");
                preluare_comanda(comanda);
                if (strcmp(comanda, "da"))
                {
                    c = 'r';
                }
                break;
            case 'r':
                break;
            default: // comanda invalida
                printf(RED "Comandă invalidă. " RESET "Apăsați " BLU "'h' + ENTER" RESET " pentru a afișa lista cu comenzi valide.\n");
                // flush_input();
                break;
        }
    } while(c != 'q');
}

void mesaj_bun_venit(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT nume, prenume "
            "FROM UTILIZATORI "
            "WHERE id_user=%ld", id_user);
    rez_inter = interogare(conn, text_interogare);
    rand_inter = mysql_fetch_row(rez_inter);
    printf("Bun venit, " CYN "%s %s" RESET "!\n", rand_inter[1], rand_inter[0]);
    mysql_free_result(rez_inter);
}

void afisare_ajutor()
{
    printf(MAG "\nAJUTOR ZORK\n" RESET);
    printf("******************************************************************************************\n");
    printf(BLU "Modul DEPLASARE\n" RESET);
    printf("Pentru a comuta la modul de DEPLASARE, introduceți comanda \"depl\" în modul de COMANDĂ.\n");
    printf("Jocul suportă următoarele tipuri de comenzi de deplasare:\n");
    printf("w --->>> mergi înainte\n");
    printf("s --->>> mergi înapoi\n");
    printf("a --->>> îndreaptă-te spre stânga\n");
    printf("d --->>> îndreaptă-te spre dreapta\n");
    printf("k --->>> afișează direcția în care vă îndreptați și camera în care vă aflați\n");
    printf("h --->>> afișează acest meniu explicativ\n");
    printf("q --->>> ieșire din joc\n\n");
    printf(BLU "Modul COMANDĂ\n" RESET);
    printf("Pentru a comuta la modul de COMANDĂ, apăsați tastele 'c' + ENTER în modul de DEPLASARE.\n");
    printf("Jocul suportă următoarele tipuri de comenzi în acest mod:\n");
    printf("dsc --->>> afișează descrierea camerei curente\n");
    printf("viz --->>> descrie ce se vede în direcția de privire a jucătorului\n");
    printf("obj --->>> afișează toate obiectele aflate în camera curentă\n");
    printf("gzn --->>> afișează toate obiectele aflate în ghiozdanul jucătorului\n");
    printf("ia <nume_obiect> --->>> pune în rucsac obiectul cu numele trimis ca parametru\n");
    printf(RED "\tNumele obiectului trebuie scris cu minuscule, fără diacritice! (apare în descriere)\n" RESET);
    printf("ajt --->>> afișează acest meniu explicativ\n");
    printf("qt --->>> ieșire din joc\n");
    printf("******************************************************************************************\n\n");
}

void preluare_comanda(char comanda[])
{
    fgets(comanda, MAX_CHAR_WIDTH - 1, stdin);
    if (strlen(comanda))
    {
        comanda[strlen(comanda) - 1] = '\0';
    }
    else
    {
        comanda[0] = '\0';
    }
}

int procesare_comanda(MYSQL* conn, char comanda[], long id_user)
{
    // trecere in modul DEPLASARE
    if (strcmp(comanda, "depl") == 0)
    {
        return 0;
    }

    // descrierea camerei curente
    if (strcmp(comanda, "dsc") == 0)
    {
        afisare_descriere_camera(conn, id_user);
        return 0;
    }

    // descrie ce anume vede jucatorul in fata (adica ce camera urmeaza)
    if (strcmp(comanda, "viz") == 0)
    {
        descriere_directie_vizibila(conn, id_user);
        return 0;
    }

    // afisarea listei de obiecte din camera curenta
    if (strcmp(comanda, "obj") == 0)
    {
        afisare_obiecte_camera(conn, id_user);
        return 0;
    }

    // afisarea listei de obiecte din ghiozdanul jucătorului
    if (strcmp(comanda, "gzn") == 0)
    {
        afisare_ghiozdan(conn, id_user);
        return 0;
    }

    // ia un obiect din camera (si pune-l in ghiozdan)
    char* pch;
    pch = strtok(comanda, " "); // comanda efectiva ("ia")
    if (strcmp(pch, "ia") == 0)
    {
        pch = strtok(NULL, " "); // numele obiectului care trebuie pus in ghiozdan
        adaugare_in_ghiozdan(conn, id_user, pch);
        return 0;
    }

    // ajutor
    if (strcmp(comanda, "ajt") == 0)
    {
        afisare_ajutor();
        return 0;
    }

    // iesire din joc
    if (strcmp(comanda, "qt") == 0)
    {
        printf("Sigur doriți să ieșiți din joc? Starea dvs. actuală este salvată automat. Introduceți \"" RED "da" RESET "\" pentru confirmare: ");
        preluare_comanda(comanda);
        if (strcmp(comanda, "da") == 0)
        {
            return -1;
        }
        return 0;
    }

    printf(RED "Comandă invalidă. " RESET "Introduceți \"" BLU "ajt" RESET "\" pentru a afișa lista cu comenzi valide.\n");
    return -2;
}

int verificare_castig(MYSQL* conn, long id_user)
{
    // verificam daca utilizatorul se afla in camera de castig
    if (preluare_id_camera(conn, id_user) != ID_CAMERA_CASTIG)
    {
        return 0;
    }

    // apoi verificam daca utilizatorul are asupra lui obiectul cu care jocul se castiga
    sprintf(text_interogare, "SELECT 1 "
            "FROM GHIOZDAN "
            "WHERE id_user=%ld "
            "AND id_obiect=%d", id_user, ID_OBIECT_CASTIG);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0)
    {
        mysql_free_result(rez_inter);
        return 0;
    }

    mysql_free_result(rez_inter);
    return 1;
}