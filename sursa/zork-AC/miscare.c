//
// Created by cosmin on 23.05.2017.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql.h>
#include "baza_de_date.h"
#include "miscare.h"
#include "accesori.h"
#include "zork_utils.h"

// folosite pentru interogari
char text_interogare[MAX_QUERY_LENGTH];
MYSQL_RES* rez_inter;
MYSQL_ROW rand_inter;

// functiile de deplasare prin camere
void inainte(MYSQL* conn, long id_user)
{
    char directie = preluare_orientare(conn, id_user);
    int camera_curenta = preluare_id_camera(conn, id_user);
    int camera_noua;
    char etaj_curent = preluare_etaj(conn, id_user);
    char etaj_nou;

    sprintf(text_interogare, "SELECT c2.id_camera "
                            "FROM CAMERE c1 JOIN CAMERE c2 "
                            "ON (c1.id_camera_%c=c2.id_camera) "
                            "WHERE c1.id_camera=%d", directie, camera_curenta);
    rez_inter = interogare(conn, text_interogare);

    // fundatura
    if (mysql_num_rows(rez_inter) == 0)
    {
        printf(RED "Fundătură!" RESET " Nu puteți înainta.\n");
        mysql_free_result(rez_inter);
        return;
    }

    // actualizam camera in care se afla utilizatorul
    rand_inter = mysql_fetch_row(rez_inter);
    camera_noua = (int)strtol(rand_inter[0], &cptr_inter, 10);
    mysql_free_result(rez_inter);
    actualizare_camera(conn, id_user, camera_noua);

    // verificam daca a urcat / coborat un etaj
    etaj_nou = preluare_etaj(conn, id_user);
    if (etaj_nou != etaj_curent)
    {
        schimbare_etaj(etaj_curent, etaj_nou);
    }

    // afisam descrierea camerei in care am intrat
    afisare_descriere_camera(conn, id_user);

    // verificam conditia de castig
    if (verificare_castig(conn, id_user))
    {
        // daca jucatorul a castigat, ii golim ghiozdanul
        sprintf(text_interogare, "DELETE FROM GHIOZDAN "
                                "WHERE id_user=%ld", id_user);
        rez_inter = interogare(conn, text_interogare);
        mysql_free_result(rez_inter);

        // apoi il punem in pozitia de inceput, orientat spre nord
        sprintf(text_interogare, "UPDATE UTILIZATORI "
                "SET id_camera=1, orientare='N' "
                "WHERE id_user=%ld", id_user);
        rez_inter = interogare(conn, text_interogare);
        mysql_free_result(rez_inter);

        printf("Dl. prof. Amarandei vă este foarte recunoscător că i-ați adus DVD-ul cu Windows Vista, cu scopul de a-l "
                       "distruge definitiv. Astfel, v-ați îndeplinit prima sarcină din acest joc (și ultima, pentru "
                       "această versiune demonstrativă).\n");
        printf(GRN "FELICITĂRI! " RESET "Ați câștigat!\n");
        deconectare_BD(conn);
        exit(EXIT_SUCCESS);
    }
}

void inapoi(MYSQL* conn, long id_user)
{
    char directie = preluare_orientare(conn, id_user);
    char directie_noua = '\0';

    switch (directie)
    {
        case 'N':
            directie_noua = 'S';
            break;
        case 'E':
            directie_noua = 'V';
            break;
        case 'S':
            directie_noua = 'N';
            break;
        case 'V':
            directie_noua = 'E';
            break;
        default:
            break;
    }

    // reimprospatam campul vizual
    actualizare_orientare(conn, id_user, directie_noua);
    reimprospatare_vedere(conn, id_user, directie_noua);
}

void dreapta(MYSQL* conn, long id_user)
{
    char directie = preluare_orientare(conn, id_user);
    char directie_noua = '\0';

    switch (directie)
    {
        case 'N':
            directie_noua = 'E';
            break;
        case 'E':
            directie_noua = 'S';
            break;
        case 'S':
            directie_noua = 'V';
            break;
        case 'V':
            directie_noua = 'N';
            break;
        default:
            break;
    }

    // reimprospatam campul vizual
    actualizare_orientare(conn, id_user, directie_noua);
    reimprospatare_vedere(conn, id_user, directie_noua);
}

void stanga(MYSQL* conn, long id_user)
{
    char directie = preluare_orientare(conn, id_user);
    char directie_noua = '\0';

    switch (directie)
    {
        case 'N':
            directie_noua = 'V';
            break;
        case 'E':
            directie_noua = 'N';
            break;
        case 'S':
            directie_noua = 'E';
            break;
        case 'V':
            directie_noua = 'S';
            break;
        default:
            break;
    }

    // reimprospatam campul vizual
    actualizare_orientare(conn, id_user, directie_noua);
    reimprospatare_vedere(conn, id_user, directie_noua);
}

void actualizare_orientare(MYSQL* conn, long id_user, char orientare)
{
    sprintf(text_interogare, "UPDATE UTILIZATORI "
                            "SET orientare='%c' "
                            "WHERE id_user=%ld", orientare, id_user);
    rez_inter = interogare(conn, text_interogare);
    mysql_free_result(rez_inter);
}

void determinare_orientare(char directie, char text_buffer[])
{
    switch (directie)
    {
        case 'N':
            strcpy(text_buffer, "NORD");
            break;
        case 'E':
            strcpy(text_buffer, "EST");
            break;
        case 'S':
            strcpy(text_buffer, "SUD");
            break;
        case 'V':
            strcpy(text_buffer, "VEST");
            break;
        default:
            break;
    }
}

// afiseaza ce se afla in campul vizual determinat de orientarea curenta (adica descrie ce e in fata)
void reimprospatare_vedere(MYSQL* conn, long id_user, char directie)
{
    char text_directie[5];
    determinare_orientare(directie, text_directie);
    printf("Vă îndreptați spre " CYN "%s" RESET ".\n", text_directie);
    descriere_directie_vizibila(conn, id_user);
}

void actualizare_camera(MYSQL* conn, long id_user, int camera)
{
    sprintf(text_interogare, "UPDATE UTILIZATORI "
                            "SET id_camera='%d' "
                            "WHERE id_user=%ld", camera, id_user);
    interogare(conn, text_interogare);
}

void schimbare_etaj(char etaj_vechi, char etaj_nou)
{
    if (etaj_vechi == 'P') // urcam de la parter
    {
        printf("Ați urcat de la parter la etajul %c.\n", etaj_nou);
    }
    else if (etaj_nou == 'P') // coborare la parter
    {
        printf("Ați coborât de la etajul %c la parter.\n", etaj_vechi);
    }
    else if (etaj_vechi < etaj_nou) // urcare
    {
        printf("Ați urcat de la etajul %c la etajul %c.\n", etaj_vechi, etaj_nou);
    }
    else if (etaj_vechi > etaj_nou) // coborare
    {
        printf("Ați coborât de la etajul %c la etajul %c.\n", etaj_vechi, etaj_nou);
    }
}