//
// Created by cosmin on 23.05.2017.
//

#ifndef ZORK_AC_MISCARE_H
#define ZORK_AC_MISCARE_H

#include <mysql.h>

void inainte(MYSQL* conn, long id_user);
void inapoi(MYSQL* conn, long id_user);
void dreapta(MYSQL* conn, long id_user);
void stanga(MYSQL* conn, long id_user);
void actualizare_orientare(MYSQL* conn, long id_user, char orientare);
void determinare_orientare(char directie, char text_buffer[]);
void reimprospatare_vedere(MYSQL* conn, long id_user, char directie);
void actualizare_camera(MYSQL* conn, long id_user, int camera);
void schimbare_etaj(char etaj_curent, char etaj_nou);

#endif //ZORK_AC_MISCARE_H
