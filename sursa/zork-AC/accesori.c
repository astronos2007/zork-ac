//
// Created by cosmin on 24.05.2017.
//

#include <mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include "accesori.h"
#include "baza_de_date.h"
#include "zork_utils.h"
#include "miscare.h"

// folosite pentru interogari
char text_interogare[MAX_QUERY_LENGTH];
MYSQL_RES* rez_inter;
MYSQL_ROW rand_inter;

int preluare_id_camera(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT id_camera "
                            "FROM UTILIZATORI "
                            "WHERE id_user=%ld", id_user);
    rez_inter = interogare(conn, text_interogare);
    rand_inter = mysql_fetch_row(rez_inter);
    int id_camera = (int)strtol(rand_inter[0], &cptr_inter, 10);
    mysql_free_result(rez_inter);
    return id_camera;
}

char preluare_orientare(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT orientare "
                            "FROM UTILIZATORI "
                            "WHERE id_user=%ld", id_user);
    rez_inter = interogare(conn, text_interogare);
    rand_inter = mysql_fetch_row(rez_inter);
    char orientare = rand_inter[0][0];
    mysql_free_result(rez_inter);
    return orientare;
}

char preluare_etaj(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT etaj "
                            "FROM UTILIZATORI JOIN CAMERE USING(id_camera) "
                            "JOIN DETALII_CAMERE USING(id_camera) "
                            "WHERE id_user=%ld", id_user);
    rez_inter = interogare(conn, text_interogare);
    rand_inter = mysql_fetch_row(rez_inter);
    char etaj = rand_inter[0][0];
    mysql_free_result(rez_inter);
    return etaj;
}

void afisare_descriere_camera(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT descriere "
                            "FROM UTILIZATORI JOIN CAMERE USING(id_camera) "
                            "JOIN DETALII_CAMERE USING(id_camera) "
                            "WHERE id_user=%ld", id_user);
    rez_inter = interogare(conn, text_interogare);
    rand_inter = mysql_fetch_row(rez_inter);
    printf("%s\n", rand_inter[0]);
    mysql_free_result(rez_inter);
}

// afiseaza descrierea scurta a camerei din fata (ce se vede din spatele usii)
void descriere_directie_vizibila(MYSQL* conn, long id_user)
{
    char orientare = preluare_orientare(conn, id_user);
    sprintf(text_interogare, "SELECT descriere_usa "
            "FROM DETALII_CAMERE "
            "WHERE id_camera=(SELECT COALESCE(id_camera_%c, -1) "
                            "FROM UTILIZATORI JOIN CAMERE USING(id_camera)"
                            "WHERE id_user=%ld)", orientare, id_user);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0) // fundatura
    {
        printf("În fața dvs. nu se află decât un perete.\n");
        mysql_free_result(rez_inter);
        return;
    }
    rand_inter = mysql_fetch_row(rez_inter);
    printf("%s\n", rand_inter[0]);
    mysql_free_result(rez_inter);
}

void afisare_obiecte_camera(MYSQL* conn, long id_user)
{
    // obiectele care se afla in camera curenta trebuie afisate in functie de ce se gaseste in ghiozdanul jucatorului
    // adica daca un obiect este in ghiozdan (este luat), nu trebuie sa mai apara ca fiind in camera
    sprintf(text_interogare, "SELECT o.nume, o.descriere "
                            "FROM UTILIZATORI u JOIN CAMERE c USING(id_camera) "
                            "JOIN OBIECTE o USING(id_camera) "
                            "WHERE u.id_user=%ld " // preluam obiectul din lista aferenta camerei respective
                            "AND o.id_obiect NOT IN (SELECT id_obiect " // ne asiguram ca nu se afla deja in ghiozdan
                                                    "FROM GHIOZDAN "
                                                    "WHERE id_user=u.id_user)", id_user);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0)
    {
        printf("Nu există niciun obiect interesant în această cameră.\n");
        return;
    }
    printf("În camera în care vă aflați, vedeți următoarele obiecte:\n");
    while ((rand_inter = mysql_fetch_row(rez_inter)))
    {
        printf(YEL "\t%s" RESET " -> %s\n", rand_inter[0], rand_inter[1]);
    }
    mysql_free_result(rez_inter);
}

void afisare_ghiozdan(MYSQL* conn, long id_user)
{
    sprintf(text_interogare, "SELECT o.nume, o.descriere "
                            "FROM UTILIZATORI u JOIN GHIOZDAN g USING(id_user) "
                            "JOIN OBIECTE o USING(id_obiect) "
                            "WHERE u.id_user=%ld ", id_user);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0)
    {
        printf("Nu aveți niciun obiect în ghiozdan.\n");
        return;
    }
    printf("În ghiozdan, aveți următoarele obiecte:\n");
    while ((rand_inter = mysql_fetch_row(rez_inter)))
    {
        printf(CYN "\t%s" RESET " -> %s\n", rand_inter[0], rand_inter[1]);
    }
    mysql_free_result(rez_inter);
}

void adaugare_in_ghiozdan(MYSQL* conn, long id_user, char* nume_obiect)
{
    // verificam mai intai daca obiectul exista in baza de date
    sprintf(text_interogare, "SELECT id_obiect "
                            "FROM OBIECTE "
                            "WHERE nume='%s'", nume_obiect);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0)
    {
        printf(RED "Eroare!" RESET " Obiectul " YEL "\"%s\"" RESET " nu există.\n", nume_obiect);
        mysql_free_result(rez_inter);
        return;
    }
    rand_inter = mysql_fetch_row(rez_inter);
    int id_obiect = (int)strtol(rand_inter[0], &cptr_inter, 10);
    mysql_free_result(rez_inter);

    // apoi verificam ca acel obiect sa fie din camera curenta
    sprintf(text_interogare, "SELECT c.id_camera "
            "FROM UTILIZATORI u JOIN CAMERE c USING(id_camera) "
            "JOIN OBIECTE o USING (id_camera) "
            "WHERE u.id_user=%ld "
            "AND u.id_camera=c.id_camera "
            "AND o.id_obiect=%d", id_user, id_obiect);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) == 0)
    {
        printf(RED "Eroare!" RESET " Obiectul \"" YEL "%s" RESET "\" nu există în camera curentă.\n", nume_obiect);
        mysql_free_result(rez_inter);
        return;
    }
    mysql_free_result(rez_inter);

    // verificam daca obiectul exista deja in ghiozdan
    sprintf(text_interogare, "SELECT id_obiect "
                            "FROM OBIECTE JOIN GHIOZDAN USING(id_obiect) "
                            "WHERE id_user=%ld "
                            "AND id_obiect=%d", id_user, id_obiect);
    rez_inter = interogare(conn, text_interogare);
    if (mysql_num_rows(rez_inter) != 0)
    {
        printf(RED "Eroare!" RESET " Obiectul \"" YEL "%s" RESET "\" nu există în camera curentă.\n", nume_obiect);
        mysql_free_result(rez_inter);
        return;
    }

    // daca totul este in regula, adaugam obiectul in ghiozdan, daca nu cumva exista deja
    sprintf(text_interogare, "INSERT INTO GHIOZDAN VALUES(%ld, %d) ", id_user, id_obiect);
    rez_inter = interogare(conn, text_interogare);
    mysql_free_result(rez_inter);
    printf("Obiectul \"" YEL "%s" RESET "\" a fost pus în ghiozdan.\n", nume_obiect);
}

void afisare_orientare(MYSQL* conn, long id_user)
{
    char text_directie[5];
    determinare_orientare(preluare_orientare(conn, id_user), text_directie);
    printf("Vă îndreptați spre " CYN "%s" RESET ". ", text_directie);
    printf("Vă aflați în camera " YEL "%d" RESET ".\n", preluare_id_camera(conn, id_user));
}