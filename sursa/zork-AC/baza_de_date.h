//
// Created by cosmin on 21.05.2017.
//

#ifndef ZORK_AC_BAZA_DE_DATE_H
#define ZORK_AC_BAZA_DE_DATE_H

#include <mysql.h>

#define MAX_QUERY_LENGTH 256
#define MAX_USER_LENGTH 25
#define MAX_PASSWORD_LENGTH 20
#define MAX_NAME_LENGTH 45
#define MAX_SURNAME_LENGTH 45

#define ID_CAMERA_CASTIG 16 // C0-5
#define ID_OBIECT_CASTIG 1 // DVD-ul cu Windows Vista

// folosite pentru interogari
char text_interogare[MAX_QUERY_LENGTH];
MYSQL_RES* rez_inter;
MYSQL_ROW rand_inter;
char* cptr_inter;

MYSQL* conectare_BD();
void deconectare_BD(MYSQL* conn);
void eroare(MYSQL* conn);
MYSQL_RES* interogare(MYSQL* conn, const char* cod_interogare);

#endif //ZORK_AC_BAZA_DE_DATE_H
