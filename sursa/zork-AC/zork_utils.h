//
// Created by cosmin on 22.05.2017.
//

#ifndef ZORK_AC_ZORK_UTILS_H
#define ZORK_AC_ZORK_UTILS_H

#include <mysql.h>

// pentru afisarea caracterelor colorate
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define RESET "\x1B[0m"

void flush_input();
void joc(MYSQL* conn, long id_user);
void mesaj_bun_venit(MYSQL* conn, long id_user);
void afisare_ajutor();
void preluare_comanda(char comanda[]);
int procesare_comanda(MYSQL* conn, char comanda[], long id_user);
int preluare_id_camera(MYSQL* conn, long id_user);
int verificare_castig(MYSQL* conn, long id_user);


#endif //ZORK_AC_ZORK_UTILS_H
