#include <mysql.h>
#include <stdio.h>
#include <stdlib.h>

#include "baza_de_date.h"
#include "initializare.h"
#include "zork_utils.h"

int main(void)
{
    // conectare la baza de date
    MYSQL* BD = conectare_BD();

    // initializare (autentificare / inregistrare + mesaj de bun venit)
    long id_user = init(BD);

    // pornire joc
    joc(BD, id_user);

    // inchidere conexiune cu baza de date
    deconectare_BD(BD);
}