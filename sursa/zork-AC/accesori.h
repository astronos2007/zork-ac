//
// Created by cosmin on 24.05.2017.
//

#ifndef ZORK_AC_ACCESORI_H
#define ZORK_AC_ACCESORI_H

#include <mysql.h>

int preluare_id_camera(MYSQL* conn, long id_user);
char preluare_orientare(MYSQL* conn, long id_user);
char preluare_etaj(MYSQL* conn, long id_user);
void afisare_descriere_camera(MYSQL* conn, long id_user);
void descriere_directie_vizibila(MYSQL* conn, long id_user);
void afisare_obiecte_camera(MYSQL* conn, long id_user);
void afisare_ghiozdan(MYSQL* conn, long id_user);
void adaugare_in_ghiozdan(MYSQL* conn, long id_user, char* nume_obiect);
void afisare_orientare(MYSQL* conn, long id_user);

#endif //ZORK_AC_ACCESORI_H
